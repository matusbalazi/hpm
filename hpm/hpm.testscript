: No-arguments-given
: Checks error printout if no arguments given
:
: The $* means "executable and options string"
: The 2>> means check error
: The "EOE" means until End Of Error
: The quotes around EOE means the string up until EOE
: will be expanded, and not taken literally.
: The != 0 means test will fail if return code is 0
: The $0 will expand to the executable name
$* >>"EOE"
Usage:
$0 <camera-parameters> <marker-parameters> <image> [-h|--help] [-v|--verbose] [-s|--show <value>] [-n|--no-fit-by-distance] [-c|--camera-position-calibration] [-t|--try-hard] [-f|--force-try-hard] [-b|--bed-reference] [-r|--reprojection-error-limit <value>]

-h, --help                         Print this help.
-v, --verbose                      Print rotation, translation, and reprojection_error of the found
                                   pose. The default is to only print the translation.
-s, --show                         <result|r|intermediate|i|all|a|none|n>. none is the default. During
                                   any pop up you may press s to write the image, or q to stop showing
                                   images, or any other key to continue.
-n, --no-fit-by-distance           Don't fit the mark detection results to only those marks who match
                                   the marks' internal distance to each other.
-c, --camera-position-calibration  Output the position of the camera in a way that can be pasted
                                   into the camera-parameters file.
-t, --try-hard                     Try harder \(but slower\) to find a good position. If one marker
                                   was slightly mis-detected, this option will make the program
                                   find decent values based on the other markers, and ignore the
                                   mis-detected one.
-f, --force-try-hard               Works like --try-hard, but will always use only the best 5 markers
                                   for position calculation, even if no markers were mis-detected.
                                   Setting both --try-hard and --force-try-hard has the same effect
                                   as only setting --force-try-hard.
-b, --bed-reference                If you have a set of markers on a flat background surface \("the
                                   bed"\), and have specified bed marker positions, type, and diameter
                                   in the marker-parameters file, then the bed reference option
                                   will make hpm calculate effector pose relative to the bed instead
                                   of relative to the camera. This can give more stable results
                                   if the camera isn't completely still between measurements, and
                                   makes measurements far less sensitive to having correct camera_translation
                                   and camera_rotation values in the camera-parameters file.
-r, --reprojection-error-limit     <number>. Default is 1.0. A lower number rejects more unceirtain
                                   results. A higher number accepts more unceirtain results.
EOE

: End-as-expected
: Checks what gets printed when run was successful
$* $src_base/example-cam-params/myExampleCamParams.xml $src_base/example-marker-params/marker-params-for-the-old-tests.xml $src_base/test-images/ball_25_84_dist_1001_08_Z.png --no-fit-by-distance --verbose --try-hard >>~/END/
/Found \d markers/
Could not identify effector markers
/\[-?\d\d?\d?\d?\.\d\d?\d?\d?, -?\d\d?\d?\d?\.\d\d?\d?\d?, -?\d\d?\d?\d?\.\d\d?\d?\d?\]/
END

: Non-existent-calibration-file
: Checks behviour when calibration coeffs file doesn't exist
$* 'nonfile' $src_base/example-marker-params/marker-params-for-the-old-tests.xml $src_base/test-images/ball_25_84_dist_1001_08_Z.png 2>>EOE != 0
Failed to load cam params file: nonfile
EOE

: Non-existent-image-file
: Checks behviour when calibration coeffs file doesn't exist
$* $src_base/example-cam-params/myExampleCamParams.xml $src_base/example-marker-params/marker-params-for-the-old-tests.xml 'noimage' 2>>EOE != 0
Could not read the image: noimage
EOE

: Swaps-image-file-and-camera-parameters
: Checks behviour when calibration coeffs file doesn't exist
$* $src_base/test-images/ball_25_84_dist_1001_08_Z.png $src_base/example-marker-params/marker-params-for-the-old-tests.xml $src_base/example-cam-params/myExampleCamParams.xml 2>>~%EOE% != 0
%Could not read camera parameters from file .+/test-images/ball_25_84_dist_1001_08_Z.png%
%.+Input file is invalid.+%
%%
EOE

: Zero-marker-diameter
: Checks behviour when the marker diameter is zero
cat <<EOI >=zero-marker-diameter.xml;
<?xml version="1.0"?>
<opencv_storage>
<marker_positions type_id="opencv-matrix">
  <rows>6</rows>
  <cols>3</cols>
  <dt>d</dt>
  <data>
     -72.4478  -125.483  0.0
      72.4478  -125.483  0.0
     144.8960       0.0  0.0
      72.4478   125.483  0.0
     -72.4478   125.483  0.0
    -144.8960       0.0  0.0
  </data>
</marker_positions>
<marker_diameter>0.0</marker_diameter>
</opencv_storage>
EOI
$* $src_base/example-cam-params/myExampleCamParams.xml zero-marker-diameter.xml $src_base/test-images/ball_25_84_dist_1001_08_Z.png 2>>EOE != 0
Need a positive effector marker diameter. Can not use 0
EOE

: Large-reprojection-error-warning
: Checks that hpm warns user about a big reprojection error
$* $src_base/example-cam-params/myExampleCamParams.xml $src_base/example-marker-params/marker-params-for-the-old-tests.xml $src_base/test-images/generated_benchmark_nr6_32_elevated_150p43_0_0_0_30_0_0_1500_white.png >>~/END/
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\]; Warning! High reprojection error: \d\d?\d?\d?.\d\d?\d?\d?\d?\d?/
END

: Finds-all-markers-nozzle-position-02
: Checks that we have a single digit error
$* $src_base/example-cam-params/myExampleCamParams.xml $src_base/example-marker-params/marker-params-for-the-old-tests.xml $src_base/test-images/nozzle_position_02.jpg -v >>~/END/
Found 6 markers
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\]/
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\]/
/.*/
END

: Finds-all-markers-nozzle-position-03
: Checks that we have a single digit error
$* $src_base/example-cam-params/myExampleCamParams.xml $src_base/example-marker-params/marker-params-for-the-old-tests.xml $src_base/test-images/nozzle_position_03.jpg -v >>~/END/
Found 6 markers
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\]/
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\]/
/.*/
END

: Finds-all-markers-nozzle-position-04
: Checks that we have a single digit error
$* $src_base/example-cam-params/myExampleCamParams.xml $src_base/example-marker-params/marker-params-for-the-old-tests.xml $src_base/test-images/nozzle_position_04.jpg -v >>~/END/
Found 6 markers
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\]/
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\]/
/.*/
END

: Finds-all-markers-nozzle-position-05
: Checks that we have a single digit error
$* $src_base/example-cam-params/myExampleCamParams.xml $src_base/example-marker-params/marker-params-for-the-old-tests.xml $src_base/test-images/nozzle_position_05.jpg -v >>~/END/
Found 6 markers
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\]/
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\]/
/.*/
END

: Finds-all-markers-testing-02
: Checks that we have a single digit error
$* $src_base/example-cam-params/myExampleCamParams.xml $src_base/example-marker-params/marker-params-for-the-old-tests.xml $src_base/test-images/testing_02.jpg -v >>~/END/
Found 6 markers
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\]/
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\]/
/.*/
END

: Finds-all-markers-calib_03
: Checks that all markers are found
$* $src_base/example-cam-params/myExampleCamParams.xml $src_base/example-marker-params/marker-params-for-the-old-tests.xml $src_base/test-images/calib_03.jpg -v >>~/END/
Found 6 markers
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/.*/
END

: Finds-all-markers-calib_x0y-217z0
: Checks that all markers are found
$* $src_base/example-cam-params/myExampleCamParams.xml $src_base/example-marker-params/marker-params-for-the-old-tests.xml $src_base/test-images/calib_x0y-217z0.jpg -v >>~/END/
Found 6 markers
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/.*/
END

: Finds-all-markers-calib_x0y-217z0_3
: Checks that all markers are found
$* $src_base/example-cam-params/myExampleCamParams.xml $src_base/example-marker-params/marker-params-for-the-old-tests.xml $src_base/test-images/calib_x0y-217z0_3.jpg -v >>~/END/
Found 6 markers
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/.*/
END

: Finds-all-markers-no-cam-pos
: Checks output when there's no cam pos in cam params file
$* $src_base/example-cam-params/myExampleCamParamsNoCamPose.xml $src_base/example-marker-params/marker-params-for-the-old-tests-2.xml $src_base/test-images/zeroed_1.jpg >>~/END/
/Warning! Did not find valid camera_rotation or camera_translation in .*\. Will try to calculate these based on the input image. The calculated values will only be valid if the nozzle was at the origin, and the markers were level with the print bed, when the image was taken./
<camera_rotation type_id="opencv-matrix">
  <!-- Based on effector pose -->
  <rows>3</rows>
  <cols>1</cols>
  <dt>d</dt>
  <data>
/    -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d? -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d? -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?/
  </data>
</camera_rotation>
<camera_translation type_id="opencv-matrix">
  <!-- Based on effector pose -->
  <rows>3</rows>
  <cols>1</cols>
  <dt>d</dt>
  <data>
/    -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d? -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d? -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?/
  </data>
</camera_translation>
END

: No-cam-pos-and-verbose
: Checks output when there's no cam pos in cam params file
$* $src_base/example-cam-params/myExampleCamParamsNoCamPose.xml $src_base/example-marker-params/marker-params-for-the-old-tests-2.xml $src_base/test-images/zeroed_1.jpg --verbose >>~/END/
/Warning! Did not find valid camera_rotation or camera_translation in .*\. Will try to calculate these based on the input image. The calculated values will only be valid if the nozzle was at the origin, and the markers were level with the print bed, when the image was taken./
Found 6 markers
<camera_rotation type_id="opencv-matrix">
  <!-- Based on effector pose -->
  <rows>3</rows>
  <cols>1</cols>
  <dt>d</dt>
  <data>
/    -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d? -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d? -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?/
  </data>
</camera_rotation>
<camera_translation type_id="opencv-matrix">
  <!-- Based on effector pose -->
  <rows>3</rows>
  <cols>1</cols>
  <dt>d</dt>
  <data>
/    -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d? -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d? -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?/
  </data>
</camera_translation>
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/\d.\d\d?\d?\d?\d?\d?\d?e?\d?\d?/
END

: Camera-position-calibration-and-verbose
: Checks output when there's no cam pos in cam params file
$* $src_base/example-cam-params/myExampleCamParams.xml $src_base/example-marker-params/marker-params-for-the-old-tests-2.xml $src_base/test-images/zeroed_1.jpg --camera-position-calibration --verbose >>~/END/
Found 6 markers
<camera_rotation type_id="opencv-matrix">
  <!-- Based on effector pose -->
  <rows>3</rows>
  <cols>1</cols>
  <dt>d</dt>
  <data>
/    -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d? -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d? -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?/
  </data>
</camera_rotation>
<camera_translation type_id="opencv-matrix">
  <!-- Based on effector pose -->
  <rows>3</rows>
  <cols>1</cols>
  <dt>d</dt>
  <data>
/    -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d? -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d? -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?/
  </data>
</camera_translation>
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/\d.\d\d?\d?\d?\d?\d?\d?e?\d?\d?/
END

: Tries-to-calibrate-cam-pos-based-on-too-bad-image-nozzle-position-01
: Checks that we have a single digit error
$* $src_base/example-cam-params/myExampleCamParams.xml $src_base/example-marker-params/marker-params-for-the-old-tests.xml $src_base/test-images/nozzle_position_02.jpg --camera-position-calibration >>~%END%
%Error: Effector reprojection error was \d.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?, and bed reprojection error was nan. That is worse than the limit \d.?\d?\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?. Therefore we can't find good camera_rotation and camera_translation values. A likely cause for the high reprojection error is that the configured camera_matrix, distortion_coefficients, and/or marker positions don't match up well enough with what's found on the image. Another cause might be that the marker detector algorithm makes a mistake. Try re-running with '--show all' to verify if this is the case.%
END

: One-marker-completely-covered-over-case-1
: Checks that we still get a low reprojection error
$* $src_base/example-cam-params/loDistCamParams2_old_for_tests.xml $src_base/example-marker-params/my-marker-params.xml $src_base/test-images/try_hard_0001.jpg --try-hard --verbose >>~/END/
Found 6 markers
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/0\..*/
Used 5 identified effector markers
END

: One-marker-completely-covered-over-case-5
: Checks that we still get a low reprojection error
$* $src_base/example-cam-params/loDistCamParams2_old_for_tests.xml $src_base/example-marker-params/my-marker-params.xml $src_base/test-images/try_hard_0005.jpg --try-hard --verbose >>~/END/
Found 6 markers
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/0\..*/
Used 5 identified effector markers
END

: One-marker-completely-covered-over-case-6
: Checks that we still get a low reprojection error
$* $src_base/example-cam-params/loDistCamParams2_old_for_tests.xml $src_base/example-marker-params/my-marker-params.xml $src_base/test-images/try_hard_0006.jpg --try-hard --verbose >>~/END/
Found 5 markers
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/0\..*/
Used 5 identified effector markers
END

: One-marker-completely-covered-over-case-7
: Checks that we still get a low reprojection error
$* $src_base/example-cam-params/loDistCamParams2_old_for_tests.xml $src_base/example-marker-params/my-marker-params.xml $src_base/test-images/try_hard_0007.jpg --try-hard --verbose >>~/END/
Found 5 markers
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/0\..*/
Used 5 identified effector markers
END

: Bed-markers-try-hard-case-1
: Checks that we use 5 bed markers in a difficult case
$* $src_base/example-cam-params/loDistCamParams2_old_for_tests.xml $src_base/example-marker-params/my-marker-params.xml $src_base/test-images/bed_markers_test_try_hard.jpg --try-hard --verbose --bed-reference --reprojection-error-limit 0.27 >>~/END/
Found 6 markers
Found 6 markers
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/0\..*/
Used 5 identified effector markers
Used 5 identified bed markers
/Effector reprojection error: 0\..*/
/Bed reprojection error: 0\..*/
END

: Exceptional-bad-bed-case-1
: Checks that 7 ellipses with less than 6 valid ones doesn't crash hpm
$* $src_base/example-cam-params/loDistCamParams2_old_for_tests.xml $src_base/example-marker-params/my-marker-params.xml $src_base/test-images/exception-bedref-0.jpg --verbose --bed-reference --try-hard >>~/END/
Found 6 markers
Found 5 markers
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/\[-?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?, -?\d\d?\d?\d?.\d\d?\d?\d?\d?\d?\d?e?-?\d?\d?\]/
/0\..*/
Used 6 identified effector markers
Used 5 identified bed markers
/Effector reprojection error: 0\..*/
/Bed reprojection error: 0\..*/
END
